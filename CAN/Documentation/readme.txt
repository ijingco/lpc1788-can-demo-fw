CAN demo firmware using LPC1788

1. Press x to Enter/Exit CAN Mode

2. Every 1sec, transmit test message(�ambient condition�) in � J1939� format
	Content of test message:
	[18 FE F5 4A 00 02 04 06 08 0A 0C 0E]
	[18 FE F5] � Message code for �ambient condition�
	[4A] � source/device id default value is 4A.
 		can be change by pressing �y�(cycles from 4A to 6F)
	[00 02 04 06 08 0A 0C 0E] � dummy data

3. Every 10ms, all received can messages will be added to logs(UART0)

2. Press y to change device id(cycles from 4A to 6F)