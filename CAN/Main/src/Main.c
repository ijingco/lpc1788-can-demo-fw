#include "lpc177x_8x_can.h"
#include "lpc177x_8x_timer.h"
#include "lpc177x_8x_uart.h"
#include "lpc177x_8x_pinsel.h"
#include "board.h"
#include <stdio.h>

#define TRANSCEIVER 1 

CAN_MSG_Type TXMsg, RXMsg; // messages for test Bypass mode
#define CAN_TX_MSG_BUFFERSIZE 16
#define CAN_RX_MSG_BUFFERSIZE 32 

CAN_MSG_Type CANRxMsgBuffer[CAN_RX_MSG_BUFFERSIZE];
uint16_t CANRxMsgBufferHead;    // index of next filled slot - if head == tail then the buffer is empty
uint16_t CANRxMsgBufferTail;    // index of next empty slot - if tail + 1 == head then the buffer is full
static uint16_t ctr1000ms = 0;


uint8_t run = 1, J1939SourceAddress = 0x4A;
static int running = 0;

typedef struct _J1939_ID
{ 
   union 
   {
      uint32_t  Value;
      struct
      {
         uint8_t   SA       :8;
         uint8_t   PDUS     :8;
         uint8_t   PDUF     :8;
         uint8_t   DP       :1;
         uint8_t   EDP      :1;
         uint8_t   Priority :3;
         uint8_t   Unused   :3;
      } BIT;

   } Data;
   
}   J1939_ID; 

typedef struct _J1939_MSG
{
    uint8_t  *data;         // Message data
    uint16_t datalength;    // data length 0 to 1785
    J1939_ID id;       // Message identifier (29 bit)
    uint32_t timestamp;

} J1939_MSG;

int QueueCANRxMsg(CAN_MSG_Type *RxMsg)
{
   uint16_t temp_index;
   int return_value = 0;

   temp_index = CANRxMsgBufferTail + 1;
   if (temp_index >= CAN_RX_MSG_BUFFERSIZE)
   {
      temp_index = 0;
   }

   if (temp_index != CANRxMsgBufferHead)
   { 
      CANRxMsgBuffer[CANRxMsgBufferTail] = *RxMsg;
      
      CANRxMsgBufferTail = temp_index;
      
      return_value = 1;
   }
   else // BUFFER IS FULL!!!
   {
      //
      // There should be an M&D counter here
      //

      return_value = 0;
   } 

   return (return_value);
}


void CAN_IRQHandler()
{

	if((CAN_ReceiveMsg(CAN_ID_1, &RXMsg)))
    {
        //sprintf(buff, "Rx: ID:%d LENGTH: DATA:%c%c%c%c%c%c%c%c\r\n", RXMsg.id, RXMsg.dataA[0], RXMsg.dataA[1], RXMsg.dataA[2], RXMsg.dataA[3], RXMsg.dataB[0], RXMsg.dataB[1], RXMsg.dataB[2], RXMsg.dataB[3]);
        /**/
        
        
        QueueCANRxMsg(&RXMsg);
        //LPC_CAN1->CMR = CAN_CMR_RRB;
    }
    //console_sendString("CANint\n\r");
        LPC_CAN1->CMR = CAN_CMR_RRB;
}
void UART0_IRQHandler(void)
{
	uint8_t buff;
    	/* Read the received data */
	if(UART_Receive(LPC_UART0, &buff, 1, NONE_BLOCKING) == 1) {

            if(buff == 'x' || buff  == 'X')
            {
                running = !running;
                if(!running)
                {
                    console_sendString("Press x to Enter/Exit CAN Mode\r\n");
                }
            }
            if(buff == 'y' || buff  == 'Y')
            {
                if(!running)
                    return;
                J1939SourceAddress++;
                if(J1939SourceAddress > 0x6F)
                    J1939SourceAddress = 0x4A;
            }
	}
}


CAN_MSG_Type *NextCANMsgToRx(void)
{
   uint16_t temp;

   // GET THE NEXT CAN MESSAGE OUT OF THE RX QUEUE AND RETURN A POINTER TO IT
   // IF THE QUEUE IS EMPTY, RETURN A NIL POINTER  
   
   if (CANRxMsgBufferHead != CANRxMsgBufferTail)
   {
      temp = CANRxMsgBufferHead;
      CANRxMsgBufferHead++;
      if (CANRxMsgBufferHead >= CAN_RX_MSG_BUFFERSIZE)
      {
         CANRxMsgBufferHead = 0;
      }

      return (&(CANRxMsgBuffer[temp]));
   }
   else
   {
      return (0);
   }
}


void HandleReceivedCANMsg( void )     
{ 
   CAN_MSG_Type   *CANRxMsg;
   uint8_t  NumMsgs = 0;
	char buff[200];
         
   CANRxMsg =  NextCANMsgToRx();
      
   while( CANRxMsg != 0 )
   {     

        sprintf(buff, "Rx CAN Msg: 0x%08x Len: %d Data: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\r\n",
            RXMsg.id,
            RXMsg.len,
            RXMsg.dataA[0],
            RXMsg.dataA[1],
            RXMsg.dataA[2],
            RXMsg.dataA[3],
            RXMsg.dataB[0],
            RXMsg.dataB[1],
            RXMsg.dataB[2],
            RXMsg.dataB[3]);
        console_sendString((uint8_t*)buff);

      ++NumMsgs;
   
      CANRxMsg =  NextCANMsgToRx();
   }
}
int main(void)
{
    TIM_TIMERCFG_Type timerCfg;
    J1939_MSG J1939TxMsg;
    int c = 0;

    console_init();

    // initialize timer
    TIM_ConfigStructInit(TIM_TIMER_MODE, &timerCfg);
    TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &timerCfg);
	
  // pinsel for CAN
#if TRANSCEIVER
	PINSEL_ConfigPin(0,  0, 1);	//CAN1 RX
	PINSEL_ConfigPin(0,  1, 1);	//CAN1 TX
#else
	PINSEL_ConfigPin(0,  21, 4);	//CAN1 RX
	PINSEL_ConfigPin(0,  22, 4);	//CAN1 TX
#endif
	PINSEL_ConfigPin (0, 4, 2);	//CAN2 RX
	PINSEL_ConfigPin (0, 5, 2);	//CAN2 TX
	
	//CAN_Init(CAN_ID_1, 125000);
	//CAN_Init(CAN_ID_2, 125000);
	CAN_Init(CAN_ID_1, 33000);
	//CAN_Init(CAN_ID_2, 33000);
	CAN_ModeConfig(CAN_ID_1, CAN_SELFTEST_MODE, ENABLE);
	CAN_SetAFMode(CAN_ACC_BP);
    NVIC_EnableIRQ(CAN_IRQn);
    CAN_IRQCmd(CAN_ID_1, CANINT_RIE, ENABLE);
    
    J1939TxMsg.id.Data.Value = 0x00;
    J1939TxMsg.id.Data.BIT.Priority = 6;
    J1939TxMsg.id.Data.BIT.DP  = 0;
    J1939TxMsg.id.Data.BIT.EDP = 0;
    J1939TxMsg.id.Data.BIT.PDUF = 0xFE;
    J1939TxMsg.id.Data.BIT.PDUS = 0xF5;
    J1939TxMsg.id.Data.BIT.SA = J1939SourceAddress;

    J1939TxMsg.datalength = 8; 
      
	RXMsg.format = 0x00;
	RXMsg.id = 0x00;
	RXMsg.len = 0x00;
	RXMsg.type = 0x00;
	RXMsg.dataA[0] = RXMsg.dataA[1] = RXMsg.dataA[2] = RXMsg.dataA[3] = 'X';
	RXMsg.dataB[0] = RXMsg.dataB[1] = RXMsg.dataB[2] = RXMsg.dataB[3] = 'X';
	
    CANRxMsgBufferHead = CANRxMsgBufferTail = 0; 
    
    console_sendString("Press x to Enter/Exit CAN Mode\r\n");
	while(1)
    {
        if(running)
        {
            TIM_Waitms(10);
            
            if(!running)
                continue;
            ctr1000ms++;
            
            if(ctr1000ms >= 100)
            {
                J1939TxMsg.id.Data.Value = 0x00;
                J1939TxMsg.id.Data.BIT.Priority = 6;
                J1939TxMsg.id.Data.BIT.DP  = 0;
                J1939TxMsg.id.Data.BIT.EDP = 0;
                J1939TxMsg.id.Data.BIT.PDUF = 0xFE;
                J1939TxMsg.id.Data.BIT.PDUS = 0xF5;
                J1939TxMsg.id.Data.BIT.SA = J1939SourceAddress;

                J1939TxMsg.datalength = 8; 
                TXMsg.format = EXT_ID_FORMAT;
                TXMsg.id = J1939TxMsg.id.Data.Value;
                TXMsg.len = J1939TxMsg.datalength;
                TXMsg.type = DATA_FRAME;
                for(c = 0; c < 4; c++)
                TXMsg.dataA[c] = c*2;
                for(c = 0; c < 4; c++)
                TXMsg.dataB[c] = c*2 + 8;

                CAN_SendMsg(CAN_ID_1, &TXMsg);
                
                ctr1000ms = 0;
            }
            HandleReceivedCANMsg();
        }
    }
}

